import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../shared/services/auth.service';
import {MustMatch} from '../../../shared/validators/must-match.validators';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  mass = Array<number>();
  url: string | ArrayBuffer = '';
  photo: File;
  error: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    for (let i = 18; i < 100; i++) {
      this.mass.push(i);
    }
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      age: ['', [Validators.required]],
      photo: [''],
      username: ['', Validators.required],
      firstName: ['', [
        Validators.required,
        Validators.pattern('\\S[0-9a-zA-Z ]{0,}'),
        Validators.maxLength(40),
        Validators.minLength(2)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.pattern('\\S[0-9a-zA-Z ]{0,}'),
        Validators.maxLength(40),
        Validators.minLength(2)
      ]],
      inputEmail: ['', [Validators.required, Validators.email]],
      password1: ['', [
        Validators.required,
        Validators.minLength(6)]],
      password2: ['', Validators.required],
      confirmLicense: [false, Validators.required],
      description: ['']
    }, {
      validator: MustMatch('password1', 'password2')
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.authService.signUp(this.form.controls, this.photo)
      .subscribe(data => {
        if (data.result === 1) {
        } else {
          this.error = data.error;
        }
      });
    this.router.navigate(['../confirm-email'],
      {queryParams: {email: this.form.controls.inputEmail.value}});
  }

  onSelectFile($event): void {
    if ($event.target.files && $event.target.files[0]) {
      const reader = new FileReader();
      this.photo = $event.target.files[0];
      reader.readAsDataURL($event.target.files[0]);

      reader.onload = (load) => {
        this.url = load.target.result;
      };
    }
  }
}
