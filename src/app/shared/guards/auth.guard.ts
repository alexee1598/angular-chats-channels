import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Injectable()
export class OnlyAuthGuardService implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(): boolean {
    if (!AuthService.getUser()) {
      this.router.navigate(['sign-in']);
      return false;
    }
    return true;
  }
}
