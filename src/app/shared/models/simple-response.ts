export interface ISimpleResponse {
  result: number;
  error: string;
}
