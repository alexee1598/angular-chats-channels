import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {IUser} from '../models/user';
import {IToken} from '../models/token';
import {AbstractControl, FormGroup} from '@angular/forms';
import {ISimpleResponse} from '../models/simple-response';

export const createUser = (data: any) => {
  return {
    id: data.id,
    username: data.username,
    first_name: data.first_name,
    last_name: data.last_name,
    age: data.age,
    photo: data.photo,
  };
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  private baseUrl = environment.production ? environment.urlBackend : 'http://localhost:8000/';

  static getUser(): IUser {
    const accessToken = this.getAccessToken();
    if (accessToken) {
      return this.parseUserFromAccessToken(accessToken);
    }
    return undefined;
  }

  private static getAccessToken(): string | undefined {
    const token = JSON.parse(window.localStorage.getItem('chat.auth'));
    if (token) {
      return token.access;
    }
    return undefined;
  }

  private static parseUserFromAccessToken(accessToken: string): IUser {
    const [, payload, ] = accessToken.split('.');
    const decoded = window.atob(payload);
    return JSON.parse(decoded);
  }

  signUp(controls: { [p: string]: AbstractControl }, photo: File): Observable<ISimpleResponse> {
    const formData = new FormData();
    console.log(controls.username.value);
    formData.append('username', controls.username.value);
    formData.append('last_name', controls.lastName.value);
    formData.append('first_name', controls.firstName.value);
    formData.append('email', controls.inputEmail.value);
    formData.append('age', controls.age.value.toString());
    formData.append('password1', controls.password1.value);
    formData.append('password2', controls.password2.value);
    formData.append('photo', photo);
    console.log(2, controls.username.value);
    return this.http.request<ISimpleResponse>('POST', `${this.baseUrl}user/sign-up/`, {body: formData});
  }

  logIn(username: string, password: string): Observable<IToken> {
    return this.http.post<IToken>(`${this.baseUrl}user/log-in`, { username, password }).pipe(
      tap(token => localStorage.setItem('chat.auth', JSON.stringify(token)))
    );
  }

  logOut(): void {
    localStorage.removeItem('chat.auth');
  }
}
