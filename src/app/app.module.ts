import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {UserDetailComponent} from './components/user-detail/user-detail.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app.routing';
import {AuthenticationModule} from './components/authentication/authentication.module';
import { ConfirmEmailComponent } from './components/authentication/confirm-email/confirm-email.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    UserDetailComponent,
    ConfirmEmailComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AuthenticationModule,
    AppRoutingModule,
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
