import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  error = false;

  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private router: Router,
              ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    this.authService.logIn(this.form.value.username.value, this.form.value.password.value)
      .subscribe(user => {
          this.router.navigateByUrl('/home');
        }, error1 => {
          console.error(error1);
        }
      );
  }

  signInWithSocial(social: string): void {
    // pass
  }
}
